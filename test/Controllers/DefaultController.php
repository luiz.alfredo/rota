<?php

namespace Test\Controllers;

use Rota\Controller;

class DefaultController extends Controller {
    public function authorize($action){
        switch ($action){
            case 'forbidden': return false;
            default: return true;
        }
    }

    public function actionDefault(){
        echo "Default Action";
    }

    public function actionHello(){
        echo "World!";
    }
}
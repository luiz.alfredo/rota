<?php
declare(strict_types=1);

namespace Test;

use Exception;
use Rota\Config;
use Rota\Controller;
use PHPUnit\Framework\TestCase;

final class RotaTest extends TestCase
{

    public function setUp() : void {
        $config = new Config;
        $config->controllerNamespace = "Test\\Controllers";
        Controller::configure($config);
    }

    public function testDefaultRoute(): void
    {

        $_GET['controller'] = 'default';
        $_GET['action'] = 'default';

        Controller::handle();

        $this->expectOutputString("Default Action");

    }

    public function testCustomRoute(): void
    {

        $_GET['controller'] = 'default';
        $_GET['action'] = 'hello';

        Controller::handle();

        $this->expectOutputString('World!');

    }

    public function testForbidden(): void
    {

        $_GET['controller'] = 'default';
        $_GET['action'] = 'forbidden';

        $this->expectException(Exception::class);
        Controller::handle();
        

    }

    public function testCustomControler(): void
    {

        $_GET['controller'] = 'especial';
        $_GET['action'] = 'mock';

        Controller::handle();

        $this->expectOutputString('Mocked!');

    }

    

}

<?php

namespace Rota;

use Exception;

/**
 * Classe representativa da camada de controle do sistema.
 * A partir desta classe deverão ser estendidas novas classes
 * que irão receber as requisições do sistema.
 */
class Controller {

	/**
	 * Ação a ser executada pelo controlador
	 *
	 * @var string
	 */
	public $action = "";

	public static $config = [];
	
	/**
	 * Método construtor. Aqui serão definidos métodos a serem executados antes da ação
	 * propriamente dita.
	 *
	 * @param string $action Nome da ação a ser executada
	 */
	public function __construct($action = "default"){		
		$this->action = $action;
	}

	/**
	 * Realiza a configuração do Controlador.
	 *
	 * @param array $config
	 * @return void
	 */
	public static function configure(array $config) {
		static::$config = $config;
	}

	public static function handle(){

		$controller = $_GET[static::$config['controllerParam']] ?? static::$config['controllerDefault'];
		$action     = $_GET[static::$config['actionParam']]     ?? static::$config['actionDefault'];
		
		$Controller = static::$config['controllerNamespace'] .
					'\\'. ucfirst($controller).
					static::$config['controllerSufix'];

		$app = new $Controller($action);

		return $app->run();
	}
	
	/**
	 * Executa a ação definida
	 *
	 * @return boolean Verdadeiro, em caso de sucesso na execução, u falso, caso contrário.
	 */
	public function run(){
		$action = static::$config['actionPrefix'] . ucfirst($this->action);
        if ($this->authorize($this->action)) {
        	$this->$action();
        } else {
			$this->unauthorized();	
		}		
	}
	
	/**
	 * Cria uma URL para execução de uma ação no sistema. Útil para criação de links.
	 *
	 * @param string $controller Nome do controlador que receberá a requisição
	 * @param string $action Nome da ação a ser executada
	 * @param number $id Id do objeto a ser acessado/modificado/deletado
	 * @param array $params Parâmetros adicionais da requisição
	 * @return string URL para a requisição
	 */
	public static function route($controller, $action = false, $id = false, $params = []){
		
		if (!static::$config['rewrite']){
			$params[static::$config['controllerParam']] = $controller;
			$params[static::$config['actionParam']] = $action;
			$params[static::$config['idParam']] = $id;
		}

		$query = array_map(function($k, $v){
			return "$k=".urlencode($v);
		}, array_keys($params), $params);
		
		$q = implode("&", $query);
				
		$url = static::$config['rewrite']? (
			$controller? (
				"$controller" . ( $action?
			  		"/$action" . ( $id? 
			    		"/$id" : ""
					) : "" )
			) : ""
		) : "" ;

		$url .= $q? "?$q" : "" ;

		return self::absolutePath($url);		
	}

	/**
	 * Cria uma URL para um recurso estático.
	 *
	 * @param string $url Recurso a ser roteado	
	 * @return string URL para a requisição
	 */
	public static function absolutePath($url){

		$proto = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $_SERVER['SERVER_PROTOCOL'];
		if (preg_match("/https/i", $proto))
			$proto = "https://";
		else
			$proto = "http://";
		
		$host = $_SERVER['HTTP_X_FORWARDED_HOST'] ?? $_SERVER['HTTP_HOST'];

		$url  = $proto . $host . static::$config['baseUrl'] . $url;
			
		return $url;		
	}
	
	/**
	 * Despacha imediatamente uma requisiçãoa um controlador através de um redirecionamento HTTP 301.
	 *
	 * @param string $controller Nome do controlador que receberá a requisição
	 * @param string $action Nome da ação a ser executada
	 * @param number $id Id do objeto a ser acessado/modificado/deletado
	 * @param array $params Parâmetros adicionais da requisição
	 * @return string URL para a requisição
	 */
	public static function dispatch($controller, $action = "index", $id = NULL, $params = []){
		$url = self::route($controller, $action, $id, $params);
		header("Location: $url");
		die;
	}

	/**
	 * Autoriza o usuário a executar a ação. Este método é chamado imediatamente
	 * antes de executar a ação e deve ser sobrescrito de acordo com a
	 * implementação da subclasse.
	 * @param string $action Nome da ação a ser autorizada.
	 * @return bool Verdadeiro, caso o usuário esteja autorizado, ou falso, caso contrário.
	 */
	public function authorize(string $action) : bool {
		return true;
	}

	/**
	 * Método chamado quando o usuário é impedido de executar uma ação.
	 */
	public function unauthorized(){
		throw new Exception("Acesso não autorizado.");
	}
		
}
